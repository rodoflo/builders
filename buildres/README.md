## Plataform Buildrs 

Clientes api.

### Requirements

* Java 11
* Spring Boot 2.4.1
* Gradle 6.7.1+
* Mysql:5.7

### Infraestrutura
``` bash
docker pull mysql:5.7
docker run -d -p 3306:3306 --name mysqlBuilders -e MYSQL_DATABASE=builders -e MYSQL_ROOT_PASSWORD=root mysql:5.7
Build spring Project
```

## Teste api

https://www.getpostman.com/collections/824c1fa0ca034757a637

## Decisões do projeto

- Opteei por deixar esta api sem autenticação para otimizar o teste no postman.
- Devido a simpicidade do domíno não usei classes DTO(data transformation object)