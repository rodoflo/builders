package com.rodolfogomes.buildres.service;

import com.rodolfogomes.buildres.model.Cliente;
import com.rodolfogomes.buildres.repository.ClienteRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    private ClienteRepository clienteRepository;

    public ClientService(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    public Cliente save(Cliente cliente){
        return adicionaIdade(clienteRepository.save(cliente));
    }

    public Iterable<Cliente> list() {
        return  clienteRepository.findAll(PageRequest.of(0,3))
                .map(cliente -> this.adicionaIdade(cliente));
    }

    private Cliente adicionaIdade(Cliente cliente){
        Integer idade = LocalDate.now().getYear() - cliente.getDataNascimento().getYear();
        if(idade > 0){
            cliente.setIdade("".concat(idade.toString()).concat(" anos"));
            return cliente;
        }
        Integer idadeMeses = LocalDate.now().getMonth().getValue() - cliente.getDataNascimento().getMonth().getValue();
        cliente.setIdade("".concat(idadeMeses.toString()).concat(" meses"));
        return cliente;
    }

    public Cliente buscarPorCpf(String cpf){
       return adicionaIdade(clienteRepository.queryByCpf(cpf));
    }
    public List<Cliente> buscarPorNome(String nome){
       return clienteRepository.queryByNome(nome).stream()
               .map(this::adicionaIdade).collect(Collectors.toList());
    }

    public void editar(Cliente cliente){
        clienteRepository.save(cliente);
    }

    public void editarNome(Long id,String nome) {
        clienteRepository.editarNome(id,nome);
    }

    public void deletar(long id) {
        clienteRepository.deleteById(id);
    }
}
