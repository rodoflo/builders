package com.rodolfogomes.buildres.controller;

import com.rodolfogomes.buildres.model.Cliente;
import com.rodolfogomes.buildres.service.ClientService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/clientes")
public class ClienteController {

    private ClientService clientService;

    public ClienteController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping
    public ResponseEntity<Cliente> createCliente(@RequestBody Cliente cliente){
        return ResponseEntity.ok(clientService.save(cliente));

    }

    @PutMapping
    public ResponseEntity editarCliente(@RequestBody Cliente cliente){
        clientService.editar(cliente);
        return ResponseEntity.ok().build();

    }

    @PatchMapping("{id}")
    public ResponseEntity editarNome(@PathVariable("id") Long id,@RequestBody String nome){
        clientService.editarNome(id,nome);
        return ResponseEntity.ok().build();

    }

    @GetMapping
    public Iterable<Cliente> listarClientes(){
        return clientService.list();
    }

    @GetMapping("/buscarporcpf")
    public Cliente buscarPorCpf(@RequestParam("cpf") String cpf){
       return clientService.buscarPorCpf(cpf);
    }

    @GetMapping("/buscarpornome")
    public List<Cliente> buscarPorNome(@RequestParam("nome") String nome){
        return clientService.buscarPorNome(nome);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleter(@PathVariable("id") long id){
        clientService.deletar(id);
        return ResponseEntity.ok().build();
    }
}
