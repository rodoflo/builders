package com.rodolfogomes.buildres.repository;

import com.rodolfogomes.buildres.model.Cliente;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.util.Set;

public interface ClienteRepository extends PagingAndSortingRepository<Cliente,Long> {

    Cliente queryByCpf(String cpf);
    Set<Cliente> queryByNome(String nome);

    @Modifying
    @Transactional
    @Query("UPDATE Cliente set nome =:nome where id=:id")
    int editarNome(Long id,String nome);

}
